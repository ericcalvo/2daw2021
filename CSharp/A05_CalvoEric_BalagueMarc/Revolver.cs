using System;

public class Revolver{

    public int posicioActual;
    public int posicioBala;

    public Revolver(){
        Random  rnd = new Random();
        posicioActual = rnd.Next(1, 7);
        posicioBala= rnd.Next(1,7);
    }

    public bool Disparar(){
        if(posicioActual == posicioBala){
            return true;
        }else{
            return false;
        }
    }

    public void SeguentBala(){
        if (posicioActual == 6){
            posicioActual = 1;
        }else{
            posicioActual +=1;
        }
    }

    public void ToString(){
        Console.WriteLine("El tambor esta en la posicio {0} y la bala esta en la posicio {1}", posicioActual, posicioBala);
    }
}