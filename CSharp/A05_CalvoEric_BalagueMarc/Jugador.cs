using System;

class Jugador
{  
    private int id;
    private string nom;
    private bool viu;
  
    public Jugador(int id){
        this.id = id;
        nom = "Jugador " + id;
        viu = true;
    }

    public void Disparar(Revolver revolver){
        if (revolver.Disparar()){
            viu = false;
        }
        revolver.SeguentBala();
    }

    public bool GetViu() {
        return viu;
    }


    public string GetNom() {
        return nom;
    }
}