using System;

class Joc
{  
    private Jugador[] jugadors;
    private Revolver revolver;

    public Joc(){
        int jugadors = QuantsJugadors();
        this.jugadors = new Jugador[jugadors];
        for (int numero = 0; numero < jugadors; numero++){
            this.jugadors[numero] = new Jugador(numero+1);
        }
        revolver = new Revolver();
    }

    public bool FiJoc(Jugador jugador){
        if (jugador.GetViu() == false){
            return true;
        }else {
            return false;
        }
    }

	public void Ronda() {
        for (int numero = 0; numero < jugadors.Length; numero++){
            jugadors[numero].Disparar(revolver);
            if(FiJoc(jugadors[numero])) {
                Console.WriteLine("\nEl {0} es dispara y s'ha mort", jugadors[numero].GetNom());
                numero = 100;
            }else{
                Console.WriteLine("\nEl {0} es dispara y no s'ha mort en aquesta ronda", jugadors[numero].GetNom());
            }
        }
	}

    public int QuantsJugadors() {
        Console.Write("Quantes persones? ");
        int quantitat = Convert.ToInt16(Console.ReadLine());
        if (quantitat > 0 && quantitat < 7){
            return quantitat;
        }else{
            quantitat = 6;
            return quantitat;
        }
    }
}