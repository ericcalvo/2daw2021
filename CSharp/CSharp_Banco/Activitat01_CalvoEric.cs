﻿using System;
public class Activitat01_CalvoEric{
    //Funcion p te pregunta cuanto dinero quieres ingressar i se aplica.
    public static void IngressarDinero(ref float[] dinero, int posicio, string[] nombres){
        float ingresar;
        Console.WriteLine("");
        Console.WriteLine("Has seleccionado Ingressar Dinero");
        while(1==1){
            Console.WriteLine("");
            Console.Write("Hola "+nombres[posicio]+" cuanto quieres ingressar?  ");
            ingresar=Convert.ToSingle(Console.ReadLine());
            if (ingresar < 1000){
                dinero[posicio] =  dinero[posicio] + ingresar;
                break;
            }else if(ingresar < 1){
                Console.WriteLine("El dinero introducido no puede ser negativo");
            }else{
                Console.WriteLine("El dinero introducido supera el limite admitido, porfavor pruebe de nuevo");
            }
        }
    }
    //Funcion que saca dinero del Banco
    public static void SacarDinero(ref float[] dinero, int posicio, string[] nombres){
        int sacar;
        Console.WriteLine("");
        Console.WriteLine("Has seleccionado Sacar Dinero");
        //Bucle para pedir dinero
        while(1==1){
            Console.WriteLine("");
            Console.Write("Hola "+nombres[posicio]+" cuanto quieres Sacar?  ");
            sacar=Convert.ToInt16(Console.ReadLine());
            //If si el dinero que quiere sacar es menor, saca dinero.
            if (sacar < dinero[posicio]){
                dinero[posicio] = dinero[posicio] - sacar;
                break;
            }
            //If para declarar que no se puede sacar dinero negativo
            else if(sacar <0){
                Console.WriteLine("No puedes sacar dinero negativo");
            }
            //Informa de que no puedes sacar tanto dinero si no tiene.
            else{
                Console.WriteLine("El dinero que quieres sacar no puede ser mas grande del que tienes");
            }
        }
    }
    //Funcion que Consulta el Dinero actual en el Banco.
    public static void ConsultarDinero(ref float[] dinero, int posicio, string[] nombres){
        Console.WriteLine("");
        Console.WriteLine("Has seleccionado Consultar Dinero");
        Console.WriteLine("Hola "+nombres[posicio]+" su dinero actual es: "+ dinero[posicio]);
        Console.WriteLine("");
        if (dinero[posicio] < 0){
            var numerorandom = new Random().Next(0,2);
            if (numerorandom == 0){
                Console.WriteLine("Hola, le queremos hacer una oferta de un prestamo de 500 euros para su cuenta.");
            }
            if (numerorandom == 1){
                Console.WriteLine("Esta sufriendo sin dinero se traficker y gana 300 euros en un dia ;)");
            }
            if (numerorandom == 2){
                Console.WriteLine("HEY, le interesaria un prestamo para que no este en numeros rojos? Seria un prestamo de 800€");
            }
        }else if(dinero[posicio] > 0){
            var numerorandom = new Random().Next(0,4);
            if (numerorandom == 0){
                Console.WriteLine("Nunca has querido ser popular? Comprese un coche por tan solo 6.000€, esta oferta solo estara disponible por una semana.");
            }
            if (numerorandom == 1){
                Console.WriteLine("Siempre te ha interesado cocinar? Compre una sarten por 30€, has visto todo un regalo");
            }
            if (numerorandom == 2){
                Console.WriteLine("Te gusta la informatica, siempre has querido ser mas rapido escribiendo compra un nuevo teclado en nuestra pagina www.miteclado.com");
            }
            if (numerorandom == 3){
                Console.WriteLine("Se siente triste porque le ha dejado su novia, no pasa nada aqui puedes comprar muñecas hinchables muy baratas");
            }
            if (numerorandom == 4){
                Console.WriteLine("Quiere tener mas barba, compre este gel por solo 20€ y en menos de 2 semanas notaras cambios");
            }
        } 
    }

    //La funcion principal que se ejecuta en pantalla.
    public static void Main(){
        //Listas de los nombres, los usuarios y el Dinero de cada uno
        string[] nombres = {"Eric", "Joel", "Jose"};
        string[] usuariDNI = { "444J", "555J", "666J" };
        float[] dinero= {45f,-20f,66f};
        int posicio;
        string login;
        Console.WriteLine("Bienvenido a su Banco :)");
        //Bucle para entrar en el banco
        for (int i = 0; i < 3;){
            Console.Write("Para entrar introduzca su clave/DNI Gracias: ");
            login = Convert.ToString(Console.ReadLine());
            Console.WriteLine("");
            //If, si el dni coincide con el dni que esta guardado se ejecuta
            if (usuariDNI[0] == login || usuariDNI[1] == login || usuariDNI[2] == login){

                for(posicio = 0; posicio < usuariDNI.Length; posicio++){
                    if (login == usuariDNI[posicio]){
                        break;
                    }
                }
                while(1==1){
                    //Seleccionas que opcion ejecutar y se te manda directamente ala funcion que corresponde.
                    int opcion;
                    Console.WriteLine("1. Ingresar Dinero");
                    Console.WriteLine("2. Sacar Dinero");
                    Console.WriteLine("3. Consultar Dinero");
                    Console.WriteLine("4. Cerrar Session"); // Te manda al inicio del banco donde tendras que logearte otra vez.
                    Console.Write("Que opcion quieres ejecutar? ");
                    opcion = Convert.ToInt16(Console.ReadLine());
                    i=0;
                    //Opcion 1 para ingresar dinero
                    if (opcion == 1){
                        IngressarDinero(ref dinero, posicio, nombres);
                        Console.WriteLine("Dinero Actual: "+ dinero[posicio]);
                        Console.WriteLine("");
                    }
                    //Opcion 2 para sacar dinero
                    else if(opcion == 2){
                        SacarDinero(ref dinero, posicio, nombres);
                        Console.WriteLine("Diners Actuals: "+ dinero[posicio]);
                        Console.WriteLine("");
                    }
                    //Opcion 3 para consultar dinero
                    else if(opcion == 3){
                        ConsultarDinero(ref dinero, posicio, nombres);
                        Console.WriteLine("");
                    }else if(opcion==4){
                        break;
                    }  
                }
            }
            else{
                Console.WriteLine("Su clave o DNI es incorrecta vuelva a intentar-lo");
                i++;
            }
        }
    }
}