using System;

class MainClass {
    // MENU QUE ES MOSTRA AL IDENTIFICARSE 
  public static string Menu (){

    string opcio;
    Console.WriteLine("\nQuè vols fer?");
    Console.WriteLine("Consultar caniques");
    Console.WriteLine("Cambiar caniques");
    Console.WriteLine("Consultar lloc");
    Console.WriteLine("Sortir\n");
    opcio = Console.ReadLine();
    return opcio;

  }
// FUNCIÓ QUE UTILITZEM QUAN L'USUARI DEMANA CONSULTAR LES CANIQUES QUE TE
  public static void ConsCaniques ( int i, int[] caniques){
    Console.WriteLine("\nTu tens: "+ caniques[i] + " caniques\n");

  }
// FUNCIO QUE UTILITZEM QUAN L'USUARI VOL CAMBIAR EL NUMERO DE CANIQUES
  public static void CambCaniques (int i, ref int[] caniques1){
    Console.WriteLine("\nA cuantes caniques vols cambiar\n");
    int Ncaniques;
    Ncaniques = Convert.ToInt32(Console.ReadLine());
    caniques1[i] = Ncaniques;
  }
// FUNCIO QUE UTILITZEM QUAN L'USUARI VOL CONSULTAR LA SEVA PROCEDENCIA
  public static void ConsLloc (string[] llocs, int i){

    Console.WriteLine("\nTu ets de: "+ llocs[i]+"\n");

  }
// FUNCIÓ PRINCIPAL, EN LA QUAL TENIM TOTA LA INFORMACIÓ DE TOTS ELS PARTICIPANTS, I HA HON FEM LES COMPROVACIONS DE IDENTITAT
  public static void Main (string[] args){
    string opcio = "";
    string[] jugadors = {"Joan", "Nil", "Roc","Thiago","Nicio","Noa","Gerard","Nor"};
    int[] caniques = {12,3,25,31,6,11,21,9};
    string[] llocs = {"Vilanova","Canyelles","Vilanova","Sitges","Cunit","Vilanova","Kazakhstan","Cubelles"};

    Console.WriteLine("Qui ets");

    string jugador;
    jugador = Console.ReadLine();

    do{
        //AMB AQUESTS FOR COMPROVEM LA IDENTITAT DEL USUARIS I TRUQUEM A LES FUNCIONS UNA VEGADA FETA LA PETICIÓ
      for(int i = 0; i < jugadors.Length; i ++){
        opcio = Menu();
        switch(opcio){
          case "Consultar caniques": ConsCaniques(i, caniques);
            break;
          case "Cambiar caniques": CambCaniques(i, ref caniques);
          Console.WriteLine("\nAra tens: "+ caniques[i]+" caniques\n");
            break;
          case "Consultar lloc": ConsLloc(llocs, i);
            break;
          case "Sortir":
            break;
          }
      }
    }while(opcio != "Sortir");
  }
}