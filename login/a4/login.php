<?php
    session_start();
    require "llibreria.php";

    //entra al phpmyadmin
    $conn = mysqli_connect('localhost', 'ecalvo', 'ecalvo', 'ecalvo_login');

    if (!$conn) {
        die("Connection failed: " . $mysqli_connect_error);
    }

    if ($_SERVER['REQUEST_METHOD']=='POST'){
        $_SESSION["user"]=$_POST["email"];
        $_SESSION["pass"]=$_POST["contrasenya"];
        setcookie("usercookie", sha1(md5($_SESSION["user"])), time() + 365 * 24 * 60 * 60);
        setcookie("passcookie", sha1(md5($_SESSION["pass"])), time() + 365 * 24 * 60 * 60);

        //usuari y contrasenya del phpmyadmin
        $usuari = $_POST["email"];
        $contra = $_POST["contrasenya"];

        $comprovacioEmail = validacioEmail($_SESSION["user"]);
        $comprovacioContra = validacioContra($_SESSION["pass"]);

    $sql = "SELECT * FROM users WHERE user = '$usuari' AND pass = '$contra'";
	$result = $conn->query($sql);
        if (isset($_REQUEST["enviar"])){
            if ($comprovacioEmail == TRUE &&  $comprovacioContra == TRUE){
                //Comproba la contrasenya i usuari del phpmyadmin amb els escrits
                if ($result->num_rows > 0){
                    $user = $result->fetch_assoc();
                    $conn->close();
                    if ($user["Rol"]=='admin'){
                        header("Location: privada_admin.php");
                    }else if($user["Rol"]=='usuari'){
                        header("Location: privada_usuari.php");
                    }
                }else{
                    echo "Usuari o Contrasenya incorrecta";
                }
            }else {
                echo "Contrasenya o mail sense format indicat";
            }
        }else{
            header("Location: registrarse.php");
        }

    }

?>
    <!DOCTYPE html>
    <html lang="en">
        <head>
            <meta charset="UTF-8">
            <meta name="viewport" content="width=device-width, initial-scale=1.0">
        </head>
        <body>
            <form enctype="multipart/form-data" action="login.php" method="post">
                <h2>Login</h2>
                <p>Email: <input type="text" name="email"></p>
                <p>Contrasenya: <input type="password" name="contrasenya"></p>
                <p>Aceptar cookies? <input type="checkbox" name="Aceptar"></p>
                <button name="enviar" type="submit">Enviar</button>
                <button name="registrar" type="submit">Registrarse</button>

            </form>
        </body>
    </html>
