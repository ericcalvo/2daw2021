<?php
    session_start();
    require "llibreria.php";
    //Connecio a la base de dades
    $conn = mysqli_connect('localhost', 'ecalvo', 'ecalvo', 'ecalvo_login');

    if (!$conn) {
        die("Connection failed: " . $mysqli_connect_error);
    }
    
    //Entra quant fa connexio post
    if ($_SERVER['REQUEST_METHOD']=='POST'){
        $_SESSION["user"]=$_POST["email"];
        $_SESSION["pass"]=$_POST["contrasenya"];
        setcookie("usercookie", sha1(md5($_SESSION["user"])), time() + 365 * 24 * 60 * 60);
        setcookie("passcookie", sha1(md5($_SESSION["pass"])), time() + 365 * 24 * 60 * 60);

        //usuari y contrasenya del phpmyadmin
        $usuari = $_POST["email"];
        $contra = $_POST["contrasenya"];

        //comprovacio de format de contrasenya i email
        $comprovacioEmail = validacioEmail($_SESSION["user"]);
        $comprovacioContra = validacioContra($_SESSION["pass"]);

        $sql = "SELECT * FROM users WHERE user = '$usuari' AND pass = '$contra'";
	    $result = $conn->query($sql);
        if (isset($_REQUEST["login"])){
            if ($comprovacioEmail == TRUE &&  $comprovacioContra == TRUE){
                //Comproba la contrasenya i usuari del phpmyadmin amb els escrits
                if ($result->num_rows > 0){
                    $user = $result->fetch_assoc();
                    $conn->close();
                    //Depenent si ets admin o usuari entra a una pagina o una altre
                    if ($user["id_rol"]=='admin'){
                        header("Location: privada_admin.php");
                    }else if($user["id_rol"]=='usuari'){
                        header("Location: privada_usuari.php");
                    }
                }else{
                    echo "Usuari o Contrasenya incorrecta";
                }
            }else {
                echo "Contrasenya o mail sense format indicat";
            }
        }
        //Pagina per registrar un nou usuari
        if (isset($_REQUEST["registrar"])){
            header("Location: registrarse.php");
        }
        //Printa els productes a la pagina principal
        if (isset($_REQUEST["productes"])){
            // Muestra la tabla de Productos
            $tabla = "SELECT id, nom, descripcio, preu, id_user FROM productes";
            $result = $conn->prepare($tabla);
            $result->execute();
            $result->bind_result($tid, $tnom, $tdescripcio, $tpreu, $tiduser);
            
            echo "</table></br>";
            echo "<form enctype='multipart/form-data' action='imatge_login.php' method='post'>
                    <table border=2><tr><th colspan ='6'><p><b>Tots els productes</b></p></th></tr>
                    <tr><td><b>Id</b></td><td><b>Nom</b</td><td><b>Descripcio</b></td><td><b>Preu</b></td><td><b>User</b></td><td><b>Imatge</b></td>";
            while ($result->fetch()){
                $_SESSION["idProducte"] = $tid;
                echo "<tr><td>$tid</td><td>$tnom</td><td>$tdescripcio</td><td>$tpreu €</td><td>$tiduser</td><td><button name='imatge' type='submit'> Imatges </td></tr>";
            }
            echo "</form></table></br>";
             $conn->close();
        } 
        //Per buscar un producte de tots els productes de la base de dades
        if (isset($_REQUEST["buscar"])){
            $buscadorNombre = $_POST["buscador"];
            $tabla= "SELECT id, nom, descripcio, preu, id_user FROM productes WHERE nom = '$buscadorNombre'";
            $result = $conn->query($tabla);
            if($result->num_rows > 0){
                // Muestra la tabla de Productos con el buscador
                $result = $conn->prepare($tabla);
                $result->execute();
                $result->bind_result($tid, $tnom, $tdescripcio, $tpreu, $tiduser);
                
                echo "</table></br>";
                echo "<form enctype='multipart/form-data' action='imatge_login.php' method='post'>
                        <table border=2><tr><th colspan ='6'><p><b>Tots els productes</b></p></th></tr>
                        <tr><td><b>Id</b></td><td><b>Nom</b</td><td><b>Descripcio</b></td><td><b>Preu</b></td><td><b>User</b></td><td><b>Imatge</b></td>";
                while ($result->fetch()){
                    $_SESSION["idProducte"] = $tid;
                    echo "<tr><td>$tid</td><td>$tnom</td><td>$tdescripcio</td><td>$tpreu €</td><td>$tiduser</td><td><button name='imatge' type='submit'> Imatges </td></tr>";
                }
                echo "</form></table></br>";
                $conn->close();
            }else {
                echo "No hay productos con este nombre";
            }
        }
    }
?>
    <!DOCTYPE html>
    <html lang="en">
        <head>
            <meta charset="UTF-8">
            <meta name="viewport" content="width=device-width, initial-scale=1.0">
        </head>
        <body>
            <form enctype="multipart/form-data" action="login.php" method="post">
                <h2>Login</h2>
                <p>Email: <input type="text" name="email"></p>
                <p>Contrasenya: <input type="password" name="contrasenya"></p>
                <p>Aceptar cookies? <input type="checkbox" name="Aceptar"></p>
                <button name="login" type="submit">Login</button>
                <button name="registrar" type="submit">Registrarse</button>
                <button name="productes" type="submit">Veure els Productes</button></br></br>

                <form enctype="multipart/form-data" action="login.php" method="post">
                    <p>Buscador: <input type="text" name="buscador" id="buscador" placeholder="buscar">
                    <input type="submit" value="buscar" name="buscar"></p>
                </form>
            </form>
        </body>
    </html>