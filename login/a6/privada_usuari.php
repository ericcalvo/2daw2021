<?php
    session_start();
    require "llibreria.php";

    $user = $_SESSION["user"];

    if($_SERVER["REQUEST_METHOD"]=='POST'){
        $conn = mysqli_connect('localhost', 'ecalvo', 'ecalvo', 'ecalvo_login');
        if (!$conn) {
            die("Connection failed: " . $mysqli_connect_error);
        }

        //Afegir Producte
        if (isset($_REQUEST["afegirProducte"])){
            $nomProducte = $_POST['nom'];
            $descripcioProducte = $_POST['descripcio'];
            $preuProducte = $_POST['preu'];
            $sql = "INSERT INTO productes (nom, descripcio, preu, id_user) VALUES ('$nomProducte', '$descripcioProducte', '$preuProducte', '$user')";
            $result = $conn->query($sql);
            $conn->close();
        }
        
        //ModificarProducte
        if (isset($_REQUEST["modificarProducte"])){
            $idProducte = $_POST['id'];
            $nomProducte = $_POST['nom'];
            $descripcioProducte = $_POST['descripcio'];
            $preuProducte = $_POST['preu'];
            $sql = "UPDATE productes SET nom = '$nomProducte', descripcio = '$descripcioProducte', preu = '$preuProducte' WHERE id= '$idProducte'";
            $result = $conn->query($sql);
            $conn->close();
        }

        //Eliminar Producte
        if (isset($_REQUEST["eliminarProducte"])){
            $idProducte = $_POST['id'];
            $sql = "DELETE FROM productes WHERE id = '$idProducte'";
            $result = $conn->query($sql);
            $conn->close();
        }
    }

    if (isset($_SESSION["user"])){
        //Entrar a la base de dades
        $conn = mysqli_connect('localhost', 'ecalvo', 'ecalvo', 'ecalvo_login');
        if (!$conn) {
            die("Connection failed: " . $mysqli_connect_error);
        }

        //Mostra la taula de benvinguda al usuari
        $bienvenida = "SELECT * FROM users WHERE user = '$user'";
        $resultBienvenida = $conn->prepare($bienvenida);
        $resultBienvenida->execute();
        $resultBienvenida->bind_result($tuser, $tpass, $trol);
        echo "<table border=2><tr><th colspan ='4'><p><b>Les teves dades</b></p></th></tr>";
        echo "<tr><td><b>user</b></td><td><b>password</b</td><td><b>rol</b></td>";
        while ($resultBienvenida->fetch()){
            echo "<tr><td>$tuser</td><td>$tpass</td><td>$trol</td></tr>";
        }
        echo "</table></br>";

        // Mostra la taula de Productes
        $sql = "SELECT id, nom, descripcio, preu FROM productes WHERE id_user = '$user'";
        $result = $conn->prepare($sql);
        $result->execute();
        $result->bind_result($tid, $tnom, $tdescripcio, $tpreu);
        echo "</table></br>";
        echo "<form enctype='multipart/form-data' action='imatges_registrat.php' method='post'>
                    <table border=2><tr><th colspan ='6'><p><b>Tots els productes</b></p></th></tr>
                    <tr><td><b>Id</b></td><td><b>Nom</b</td><td><b>Descripcio</b></td><td><b>Preu</b></td><td><b>Imatge</b></td>";
        while ($result->fetch()){
            $_SESSION["idProducte"] = $tid;
            echo "<tr><td>$tid</td><td>$tnom</td><td>$tdescripcio</td><td>$tpreu €</td><td><button name='imatges' type='submit'> Imatges </td></tr>";
        }
        echo "</form></table></br>";
        $conn->close();
    }else {
        header("Location: http://dawjavi.insjoaquimmir.cat/ecalvo/eric/UF1/a6/login.php");
    }
    
    //Logout
    if (isset($_REQUEST["logout"])){
        $_SESSION=null;
        setcookie('usercookie', null,0,'/');
        setcookie('passcookie', null,0,'/');
        session_destroy();
        header("Location: http://dawjavi.insjoaquimmir.cat/ecalvo/eric/UF1/a6/login.php");
    }
?>

    <!DOCTYPE html>
    <html lang="en">
        <head>
            <meta charset="UTF-8">
            <meta name="viewport" content="width=device-width, initial-scale=1.0">
        </head>
        <body>
            
            <form action="privada_usuari.php" method="post">
                <input type="submit" name="logout" value="Log Out">
                <h3>Afegir producte</h3>
                <p>Nom: <input type="text" name="nom"></p>
                <p>Descripcio: <input type="text" name="descripcio"></p>
                <p>Preu: <input type="text" name="preu"></p>
                <input type="submit" name="afegirProducte" value="Afegir Producte">
            </form>

            <form action="privada_usuari.php" method="post">
                <h3>Modificar Producte</h3>
                <p>Id del Producte a modificar: <input type="text" name="id"></p>
                <p>Nom: <input type="text" name="nom"></p>
                <p>Descripcio: <input type="text" name="descripcio"></p>
                <p>Preu: <input type="text" name="preu"></p>
                <input type="submit" name="modificarProducte" value="Modificar Producte">
            </form>

            <form action="privada_usuari.php" method="post">
                <h3>Eliminar Producte</h3>
                <p>Id: <input type="text" name="id"></p>
                <input type="submit" name="eliminarProducte" value="Eliminar Producte">
            </form>
        </body>
    </html>