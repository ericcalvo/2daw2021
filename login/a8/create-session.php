<?php
session_start();
require 'stripe/init.php';
require 'llibreria.php';
\Stripe\Stripe::setApiKey('sk_test_51HosOEBwIcpul1R1TWleaJQw9fCVWiRpczjDqKCNF2gdxKDPNOf3qhw6GP8RIR6WdrlnmZ8MAtWiCyrWQC6w2Irj00VprQfS3m');

header('Content-Type: application/json');

$YOUR_DOMAIN = 'https://dawjavi.insjoaquimmir.cat';

$checkout_session = \Stripe\Checkout\Session::create([
  'payment_method_types' => ['card'],
  'line_items' => [[
    'price_data' => [
      'currency' => 'usd',
      'unit_amount' => 2000,
      'product_data' => [
        'name' => 'Stubborn Attachments',
        'images' => ["https://i.imgur.com/EHyR2nP.png"],
      ],
    ],
    'quantity' => 1,
  ]],
  'mode' => 'payment',
  'success_url' => $YOUR_DOMAIN . '/ecalvo/eric/UF1/a7/success.php',
  'cancel_url' => $YOUR_DOMAIN . '/ecalvo/eric/UF1/a7/error.php',
]);

echo json_encode(['id' => $checkout_session->id]);