<?php
    session_start();
    require "llibreria.php";

    $user = $_SESSION["user"];
    
    if (isset($_SESSION["idProducte"])){
        $idProducte[] = $_SESSION["idProducte"];
    }
    
?>
<!DOCTYPE html>
<html>
    <head>
        <title>Cistella</title>
        <script src="https://polyfill.io/v3/polyfill.min.js?version=3.52.1&features=fetch"></script>
        <script src="https://js.stripe.com/v3/"></script>
    </head>
    <body>
<?php
    if($_SERVER["REQUEST_METHOD"]=='POST'){
        //Boto per tornar a la pagina Privada
        if (isset($_REQUEST["tornar"])){
            $conn = mysqli_connect('localhost', 'ecalvo', 'ecalvo', 'ecalvo_login');
            if (!$conn) {
                die("Connection failed: " . $mysqli_connect_error);
            }
            $volverPrivada = "SELECT * FROM users WHERE user = '$user'";
            $resultVolver = $conn->query($volverPrivada);
            $rol= $resultVolver->fetch_assoc();
            $conn->close();
            if ($rol["id_rol"]=='admin'){
                header("Location: privada_admin.php");
            }else if ($rol["id_rol"]=='usuari'){
                header("Location: privada_usuari.php");
            }
        }
    }
    // Si no fas login no pots entrar en el carrito.
    if (!isset($_SESSION["user"])){
        header("Location: login.php");
    }
    echo "<h2>Carrito de ".$_SESSION["user"]."</h2>";
?>
<!-- HTML boto per tornar a la Pagina Privada -->
<form method="post">
    <p>Tornar pagina principal: <button type="submit" name="tornar">Tornar</button></p>
</form>

<?php
    $conn = mysqli_connect('localhost', 'ecalvo', 'ecalvo', 'ecalvo_login');
    if (!$conn) {
        die("Connection failed: " . $mysqli_connect_error);
    }

    //Taula que printa els productes que estan en la cistella del usuari
    if (isset($_SESSION["idProducte"])){
        $sql = "SELECT id, nom, descripcio, preu FROM productes WHERE id = '$idProducte[0]'";
        $result = $conn->prepare($sql);
        $result->execute();
        $result->bind_result($tid, $tnom, $tdescripcio, $tpreu);
        echo "</table></br>";
        echo "<form enctype='multipart/form-data' method='post'>
                <table border=2><tr><th colspan ='6'><p><b>Productes en Cistella</b></p></th></tr>
                <tr><td><b>Id</b></td><td><b>Nom</b</td><td><b>Descripcio</b></td><td><b>Preu</b></td>";
        while ($result->fetch()){
            echo "<tr><td>$tid</td><td>$tnom</td><td>$tdescripcio</td><td>$tpreu €</td></tr>";
        }
        echo "</form></table></br>";
        $conn->close();
    } else {
        echo "No hi han productes a la cistella";
    }
    
?>
        <!-- HTML Boto per pagar els productes en la cistella -->
        </br>
        <button id="checkout-button">Pagar</button>
    </body>

    <script type="text/javascript">
        // Create an instance of the Stripe object with your publishable API key
        var stripe = Stripe("pk_test_51HosOEBwIcpul1R1w0jCHSThAx0wRt9ABYtMiXhBOzHrvXFjHVAIM1PkJqH1xHnL6AJJFGjq3dKt7RvX6v6Ty4d300xseDrqJl");
        var checkoutButton = document.getElementById("checkout-button");
        checkoutButton.addEventListener("click", function () {
            fetch("create-session.php", {
                method: "POST",
            })
            .then(function (response) {
                return response.json();
            })
            .then(function (session) {
                return stripe.redirectToCheckout({ sessionId: session.id });
            })
            .then(function (result) {
                // If redirectToCheckout fails due to a browser or network
                // error, you should display the localized error message to your
                // customer using error.message.
                if (result.error) {
                    alert(result.error.message);
                }
            })
            .catch(function (error) {
                console.error("Error:", error);
            });
        });
    </script>
</html>