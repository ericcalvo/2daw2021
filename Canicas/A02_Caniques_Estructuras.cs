using System;
    
class MainClass {

    public static void Main(){
        int posicio;
        string[] noms= {"Joan", "Nil", "Roc", "Thiago", "Nico", "Noa", "Gerard", "Nor"};
        int[] caniques= { 12, 3, 25, 31, 6, 11, 21 , 9};
        string[] ciutat= {"Vilanova", "Canyelles", "Vilanova", "Sitges", "Cunit", "Vilanova", "Kazakhstan", "Cubelles"};
        string nom;
        
        while (1==1){
            Console.Write("Escribe tu nombre: ");
            nom = Convert.ToString(Console.ReadLine());
            for(posicio = 0; posicio < noms.Length; posicio++){
                if (nom == noms[posicio]){
                    SubMenu(ref caniques, noms, ciutat , posicio);
                    Console.WriteLine("");
                }
            }
            Console.WriteLine("Aquest nom no existeix");
        }
    }
    public static void SubMenu(ref int[] caniques, string[] noms, string[] ciutat, int posicio){
        string opcio;
        int CanicasSeleccionades;
        Console.WriteLine("");
        Console.WriteLine(noms[posicio]+ " ha vingut desde " + ciutat[posicio] + " y en te " + caniques[posicio] + " caniques.");
        Console.Write("Vols cambiar el numero de caniques? ");
        opcio = Convert.ToString(Console.ReadLine());

        if (opcio == "si"){
            Console.WriteLine("");
            Console.WriteLine("Has seleccionat cambiar el numero de caniques");
            Console.Write("Cuantes caniques vols tenir? ");
            CanicasSeleccionades= Convert.ToInt32(Console.ReadLine());
            caniques[posicio]= CanicasSeleccionades;
            Console.WriteLine(noms[posicio] + " ara en te " + caniques[posicio] + " caniques");
        }
    }
}